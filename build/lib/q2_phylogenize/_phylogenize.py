import os
import subprocess
import qiime2
import biom
import skbio
import shutil
from q2_types.feature_data import DNAIterator
from tempfile import TemporaryDirectory

def run(output_dir: str,
        table: biom.Table,
        sequences: DNAIterator,
        metadata: qiime2.Metadata,
        which_envir: str,
        ncl: int = 1,
        data_type: str = '16S',
        burst_cutoff: float = 0.985,
        minimum: int = 3,
        treemin: int = 5,
        pctmin: float = 0.025,
        assume_below_lod: bool = True,
        linearize: bool = False,
        env_column: str = 'env',
        dset_column: str = 'dataset',
        which_phenotype: str = 'prevalence',
        single_dset: bool = True,
        burst_dir: str = '',
        burst_bin: str = 'vsearch') -> None:
    _run(output_dir=output_dir,
         table=table,
         sequences=sequences,
         metadata=metadata,
         which_envir=which_envir,
         ncl=ncl,
         data_type=data_type,
         burst_cutoff=burst_cutoff,
         minimum=minimum,
         treemin=treemin,
         pctmin=pctmin,
         assume_below_lod=assume_below_lod,
         linearize=linearize,
         env_column=env_column,
         dset_column=dset_column,
         which_phenotype=which_phenotype,
         single_dset=single_dset,
         burst_dir=burst_dir,
         burst_bin=burst_bin,
         report_file="phylogenize-report.Rmd")

def run_prepared(output_dir: str,
        table: biom.Table,
        metadata: qiime2.Metadata,
        which_envir: str,
        ncl: int = 1,
        data_type: str = '16S',
        burst_cutoff: float = 0.985,
        minimum: int = 3,
        treemin: int = 5,
        pctmin: float = 0.025,
        assume_below_lod: bool = True,
        linearize: bool = False,
        env_column: str = 'env',
        dset_column: str = 'dataset',
        which_phenotype: str = 'prevalence',
        single_dset: bool = True,
        burst_dir: str = '',
        burst_bin: str = 'vsearch') -> None:
    _run(output_dir=output_dir,
         table=table,
         sequences=None,
         metadata=metadata,
         which_envir=which_envir,
         ncl=ncl,
         data_type=data_type,
         burst_cutoff=burst_cutoff,
         minimum=minimum,
         treemin=treemin,
         pctmin=pctmin,
         assume_below_lod=assume_below_lod,
         linearize=linearize,
         env_column=env_column,
         dset_column=dset_column,
         which_phenotype=which_phenotype,
         single_dset=single_dset,
         burst_dir=burst_dir,
         burst_bin=burst_bin,
         report_file="phylogenize-report.Rmd")

def run_test(output_dir: str,
             table: biom.Table,
             sequences: DNAIterator,
             metadata: qiime2.Metadata,
             which_envir: str,
             ncl: int = 1,
             data_type: str = '16S',
             burst_cutoff: float = 0.985,
             minimum: int = 3,
             treemin: int = 5,
             pctmin: float = 0.025,
             assume_below_lod: bool = True,
             linearize: bool = True,
             env_column: str = 'env',
             dset_column: str = 'dataset',
             which_phenotype: str = 'prevalence',
             single_dset: bool = True,
             burst_dir: str = '',
             burst_bin: str = 'vsearch') -> None:
    _run(output_dir=output_dir,
         table=table,
         sequences=sequences,
         metadata=metadata,
         which_envir=which_envir,
         ncl=ncl,
         data_type=data_type,
         burst_cutoff=burst_cutoff,
         minimum=minimum,
         treemin=treemin,
         pctmin=pctmin,
         assume_below_lod=assume_below_lod,
         linearize=linearize,
         env_column=env_column,
         dset_column=dset_column,
         which_phenotype=which_phenotype,
         single_dset=single_dset,
         burst_dir=burst_dir,
         burst_bin=burst_bin,
         report_file="test-report.Rmd")

def _run(output_dir: str,
         table: biom.Table,
         # sequences: DNAIterator,
         metadata: qiime2.Metadata,
         which_envir: str,
         ncl: int = 1,
         data_type: str = '16S',
         burst_cutoff: float = 0.985,
         minimum: int = 3,
         treemin: int = 5,
         pctmin: float = 0.025,
         assume_below_lod: bool = True,
         linearize: bool = False,
         env_column: str = 'env',
         dset_column: str = 'dataset',
         which_phenotype: str = 'prevalence',
         single_dset: bool = True,
         report_file: str = 'phylogenize-report.Rmd',
         burst_dir: str = '',
         burst_bin: str = 'vsearch',
         **kwargs) -> None:
    sample_column = metadata.id_header
    if ("sequences" in kwargs) and kwargs["sequences"]:
        _prep_biom(output_dir=output_dir,
                   table=table,
                   sequences=kwargs["sequences"],
                   metadata=metadata,
                   output_file="input.biom")
    else:
        _prep_biom(output_dir=output_dir,
                   table=table,
                   metadata=metadata,
                   output_file="input.biom")
    if burst_dir == '':
        burst_dir = os.path.dirname(shutil.which(burst_bin))
    Rcmd=(('sapply(c("phylogenize", "graphics", "stats", "methods",'
           '"grDevices", "biomformat"), function(.) library('
           'character.only=TRUE, .));'
           'phylogenize::set_data_internal(); '
           'phylogenize::render.report('
           'output_file="{output_file}", '
           'report_input="{report_file}", '
           'input_format="biom", '
           'biom_file="input.biom", '
           'burst_dir="{burst_dir}", '
           'burst_bin="{burst_bin}", '
           'in_dir="{output_dir}", '
           'out_dir="{output_dir}", '
           'ncl={ncl}, '
           'type="{data_type}", '
           'which_phenotype="{which_phenotype}", '
           'which_envir="{which_envir}", '
           'dset_column="{dset_column}", '
           'env_column="{env_column}", '
           'sample_column="{sample_column}", '
           'burst_cutoff={burst_cutoff}, '
           'assume_below_LOD={assume_below_lod_R}, '
           'linearize={linearize_R}, '
           'single_dset={single_dset_R}, '
           'minimum={minimum}, '
           'treemin={treemin}, '
           'pctmin={pctmin}, '
           'relative_out_dir=".", '
           'working_dir="{output_dir}"'
           ')'
    ).format(
        output_file="index.html",
        output_dir=output_dir,
        report_file=report_file,
        ncl=ncl,
        data_type=data_type,
        which_phenotype=which_phenotype,
        which_envir=which_envir,
        dset_column=dset_column,
        env_column=env_column,
        sample_column=sample_column,
        burst_cutoff=burst_cutoff,
        assume_below_lod_R="TRUE" if assume_below_lod else "FALSE",
        linearize_R="TRUE" if linearize else "FALSE",
        single_dset_R="TRUE" if single_dset else "FALSE",
        burst_dir=burst_dir,
        burst_bin=burst_bin,
        minimum=minimum,
        treemin=treemin,
        pctmin=pctmin
    ))
    try:
        print("Running phylogenize in R...")
        print(Rcmd)
        subprocess.run(['R',
                        '-e',
                        Rcmd], check=True)
    except subprocess.CalledProcessError as e:
        raise Exception("Error %d in R code!" % e.returncode)

def prepare_biom_input(output_dir: str,
                       table: biom.Table,
                       sequences: DNAIterator,
                       metadata: qiime2.Metadata,
                       output_file: str='test-input.biom') -> None:
    _prep_biom(output_dir, table, sequences, metadata, output_file)

def _prep_biom(output_dir: str,
               table: biom.Table,
               sequences: DNAIterator,
               metadata: qiime2.Metadata,
               output_file: str = 'test-input.biom',
               **kwargs) -> None:
    # Convert row names to actual DNA sequences
    dmap = dict()
    for s in sequences: dmap[s.metadata['id']] = str(s)
    table.update_ids(dmap, 'observation')
    mdf = metadata.to_dataframe()
    table.add_metadata(mdf.transpose().to_dict(), 'sample')
    sample_column = metadata.id_header
    biom_input = os.path.join(output_dir, output_file)
    with biom.util.biom_open(biom_input, 'w') as f:
        table.to_hdf5(f, "phylogenize_input")

