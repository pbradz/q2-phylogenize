#!/usr/bin/env python

from qiime2.plugin import Plugin, Str, Int, Float, Bool, Metadata
from q2_types.feature_data import FeatureData, Sequence
from q2_types.feature_table import FeatureTable, \
    Frequency, RelativeFrequency, PresenceAbsence

import q2_phylogenize
import q2_phylogenize._phylogenize

plugin = Plugin(
    name='phylogenize',
    version=q2_phylogenize.__version__,
    website='http://bitbucket.org/pbradz/q2-phylogenize',
    package='q2_phylogenize',
    description=('This QIIME 2 plugin allows users to find the genes '
                 'underlying microbe-to-environment associations, while '
                 'controlling for phylogeny. The plugin will generate an '
                 'interactive HTML report as well as several tables. '),
    short_description='Associate microbial genes with environments, ' +
                      'correcting for phylogeny.'
)

plugin.visualizers.register_function(
    function=q2_phylogenize._phylogenize.run,
    inputs={'table': FeatureTable[Frequency |
                                  RelativeFrequency |
                                  PresenceAbsence],
            'sequences': FeatureData[Sequence]},
    parameters={'metadata': Metadata,
                'which_envir': Str,
                'ncl': Int,
                'data_type': Str,
                'burst_cutoff': Float,
                'minimum': Int,
                'treemin': Int,
                'pctmin': Float,
                'assume_below_lod': Bool,
                'env_column': Str,
                'dset_column': Str,
                'which_phenotype': Str,
                'single_dset': Bool,
                'linearize': Bool,
                'burst_dir': Str,
                'burst_bin': Str},
    input_descriptions={
        'table': ('The table of counts, relative abundances, or presence/absence'
                  ' values per denoised ASV.'),
        'sequences': ('The actual denoised ASV sequences.')
    },
    parameter_descriptions={
        'metadata': ('The metadata table giving mappings of samples to '
                     'environments and datasets.'),
        'which_envir': ('Environment in which to calculate prevalence or '
                        'specificity. Must match annotations in metadata.'),
        'ncl': ('Number of cores to use for parallel computation.'),
        'data_type': ('Is the data 16S data analyzed with DADA2/Deblur '
                      '("16S"), or shotgun data analyzed with MIDAS '
                      '("midas")?'),
        'burst_cutoff': ('Percent identity cutoff for mapping to reference 16S'
                         ' sequences via BURST.'),
        'minimum': ('A gene must be present this many times and also absent '
                    'this many times in order to be counted.'),
        'treemin': ('A given phylum must have at least this many '
                    'representatives in order to be shown in the report.'),
        'pctmin': ('A given phylum must have at least this percentage of '
                   'its leaves present in order to be shown in the report.'),
        'linearize': ('Run a non-phylogenetic linear model. NOT RECOMMENDED '
                      'unless you have a really specific reason for doing '
                      'this, like making a comparison to the phylogenetically '
                      'aware model.'),
        'assume_below_lod': ('Assume that MIDAS species that are never'
                             'detected have a prevalence of zero? If not, '
                             'drop them from the analysis entirely'),
        'env_column': ('Name of column in metadata file containing the '
                       'environment annotations or, in the case of correlation, '
                       'the continuous variable.'),
        'dset_column': ('Name of column in metadata file containing the '
                        ' dataset annotations.'),
        'which_phenotype': ('Which phenotype to calculate ("prevalence", '
                            '"specificity", or "correlation")?'),
        'single_dset': ('If true, will assume that all samples come from a '
                        'single dataset called "dset1" no matter what, if '
                        'anything, is in "dset_column".'),
        'burst_dir': ('Directory to find the BURST/vsearch binary. Leave '
                      'empty to automatically search your PATH.'),
        'burst_bin': ('Name of the BURST/vsearch binary.')
    },
    name='Run phylogenize',
    description='Run phylogenize on a dataset (with ASVs in a separate file), yielding an interactive report.'
)

plugin.visualizers.register_function(
    function=q2_phylogenize._phylogenize.run_prepared,
    inputs={'table': FeatureTable[Frequency |
                                  RelativeFrequency |
                                  PresenceAbsence]},
    parameters={'metadata': Metadata,
                'which_envir': Str,
                'ncl': Int,
                'data_type': Str,
                'burst_cutoff': Float,
                'minimum': Int,
                'treemin': Int,
                'pctmin': Float,
                'assume_below_lod': Bool,
                'env_column': Str,
                'dset_column': Str,
                'which_phenotype': Str,
                'single_dset': Bool,
                'linearize': Bool,
                'burst_dir': Str,
                'burst_bin': Str},
    input_descriptions={
        'table': ('The table of counts, relative abundances, or presence/absence'
                  ' values per denoised ASV. Row names should be ASV names.')
    },
    parameter_descriptions={
        'metadata': ('The metadata table giving mappings of samples to '
                     'environments and datasets.'),
        'which_envir': ('Environment in which to calculate prevalence or '
                        'specificity. Must match annotations in metadata.'),
        'ncl': ('Number of cores to use for parallel computation.'),
        'data_type': ('Is the data 16S data analyzed with DADA2/Deblur '
                      '("16S"), or shotgun data analyzed with MIDAS '
                      '("midas")?'),
        'burst_cutoff': ('Percent identity cutoff for mapping to reference 16S'
                         ' sequences via BURST.'),
        'minimum': ('A gene must be present this many times and also absent '
                    'this many times in order to be counted.'),
        'treemin': ('A given phylum must have at least this many '
                    'representatives in order to be shown in the report.'),
        'pctmin': ('A given phylum must have at least this percentage of '
                   'its leaves present in order to be shown in the report.'),
        'linearize': ('Run a non-phylogenetic linear model. NOT RECOMMENDED '
                      'unless you have a really specific reason for doing '
                      'this, like making a comparison to the phylogenetically '
                      'aware model.'),
        'assume_below_lod': ('Assume that MIDAS species that are never'
                             'detected have a prevalence of zero? If not, '
                             'drop them from the analysis entirely'),
        'env_column': ('Name of column in metadata file containing the '
                       'environment annotations or, in the case of correlation, '
                       'the continuous variable.'),
        'dset_column': ('Name of column in metadata file containing the '
                        ' dataset annotations.'),
        'which_phenotype': ('Which phenotype to calculate ("prevalence", '
                            '"specificity", or "correlation")?'),
        'single_dset': ('If true, will assume that all samples come from a '
                        'single dataset called "dset1" no matter what, if '
                        'anything, is in "dset_column".'),
        'burst_dir': ('Directory to find the BURST/vsearch binary. Leave '
                      'empty to automatically search your PATH.'),
        'burst_bin': ('Name of the BURST/vsearch binary.')
    },
    name='Run phylogenize',
    description='Run phylogenize on a dataset (with ASVs as row names), yielding an interactive report.'
)

plugin.visualizers.register_function(
    function=q2_phylogenize._phylogenize.run_test,
    inputs={'table': FeatureTable[Frequency |
                                  RelativeFrequency |
                                  PresenceAbsence],
            'sequences': FeatureData[Sequence]},
    parameters={'metadata': Metadata,
                'which_envir': Str,
                'ncl': Int,
                'data_type': Str,
                'burst_cutoff': Float,
                'minimum': Int,
                'treemin': Int,
                'pctmin': Float,
                'assume_below_lod': Bool,
                'linearize': Bool,
                'env_column': Str,
                'dset_column': Str,
                'which_phenotype': Str,
                'single_dset': Bool,
                'burst_dir': Str,
                'burst_bin': Str},
    input_descriptions={
        'table': ('The table of counts, relative abundances, or presence/absence'
                  ' values per denoised ASV.'),
        'sequences': ('The actual denoised ASV sequences.')
    },
    parameter_descriptions={
        'metadata': ('The metadata table giving mappings of samples to '
                     'environments and datasets.'),
        'which_envir': ('Environment in which to calculate prevalence or '
                        'specificity. Must match annotations in metadata.'),
        'ncl': ('Number of cores to use for parallel computation.'),
        'data_type': ('Is the data 16S data analyzed with DADA2/Deblur '
                      '("16S"), or shotgun data analyzed with MIDAS '
                      '("midas")?'),
        'burst_cutoff': ('Percent identity cutoff for mapping to reference 16S'
                         ' sequences via BURST.'),
        'minimum': ('A gene must be present this many times and also absent '
                    'this many times in order to be counted.'),
        'treemin': ('A given phylum must have at least this many '
                    'representatives in order to be shown in the report.'),
        'pctmin': ('A given phylum must have at least this percentage of '
                   'its leaves present in order to be shown in the report.'),
        'assume_below_lod': ('Assume that MIDAS species that are never'
                             'detected have a prevalence of zero? If not, '
                             'drop them from the analysis entirely'),
        'linearize': ('Run a non-phylogenetic linear model. NOT RECOMMENDED '
                      'unless you have a really specific reason for doing '
                      'this, like making a comparison to the phylogenetically '
                      'aware model.'),
        'env_column': ('Name of column in metadata file containing the '
                       'environment annotations or, in the case of correlation, '
                       'the continuous variable.'),
        'dset_column': ('Name of column in metadata file containing the '
                        ' dataset annotations.'),
        'which_phenotype': ('Which phenotype to calculate ("prevalence", '
                            '"specificity", or "correlation")?'),
        'single_dset': ('If true, will assume that all samples come from a '
                        'single dataset called "dset1" no matter what, if '
                        'anything, is in "dset_column".'),
        'burst_dir': ('Directory to find the BURST/vsearch binary. Leave '
                      'empty to automatically search your PATH.'),
        'burst_bin': ('Name of the BURST/vsearch binary.')
    },
    name='Dry-run phylogenize',
    description='Test data import and report generation only (no analysis).'
)

plugin.visualizers.register_function(
    function=q2_phylogenize._phylogenize.prepare_biom_input,
    inputs={'table': FeatureTable[Frequency |
                                  RelativeFrequency |
                                  PresenceAbsence],
            'sequences': FeatureData[Sequence]},
    parameters={
        'output_file': Str,
        'metadata': Metadata,
    },
    input_descriptions={
        'table': ('The table of counts, relative abundances, or presence/absence'
                  ' values per denoised ASV.'),
        'sequences': ('The actual denoised ASV sequences.')
    },
    parameter_descriptions={
        'metadata': ('The metadata table giving mappings of samples to '
                     'environments and datasets.'),
        'output_file': ('File to which the prepared BIOM table will be '
                        'written.')
    },
    name='Prepare phylogenize input',
    description='Write a single BIOM-formatted file to disk in the input format '
    'that phylogenize expects (i.e., with integrated metadata and DNA sequences '
    'as row names).'
)
