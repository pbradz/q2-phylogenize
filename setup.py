from setuptools import setup, find_packages
import versioneer

setup(
    name="q2-phylogenize",
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    packages=find_packages(),
    author="Patrick J. H. Bradley",
    author_email="patrick.bradley@gladstone.ucsf.edu",
    description="Associate microbial genes with environments, correcting for phylogeny",
    license='MIT',
    url="http://www.phylogenize.org",
    entry_points={
        'qiime2.plugins':
        ['q2-phylogenize=q2_phylogenize.plugin_setup:plugin']
    },
    package_data={'q2_phylogenize': ['assets/index.html', 'citations.bib']},
    zip_safe=False,
)
