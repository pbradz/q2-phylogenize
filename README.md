[TOC]

# About q2-phylogenize

q2-phylogenize is a plugin for the popular metagenomics workflow tool, [QIIME 2](https://qiime2.org/), that enables [*phylogenize*](https://phylogenize.org) to be used in these analysis pipelines.

*phylogenize* allows users to link microbial genes to environments, while accounting for phylogeny. More information can be found at its [website](https://phylogenize.org) or at its [Bitbucket repository](https://bitbucket.org/pbradz/phylogenize/).

# Requirements

 * [phylogenize](https://bitbucket.org/pbradz/phylogenize) (follow instructions for installation for use with QIIME 2)
 * QIIME 2 (2019.1 or later)

# News

May 7, 2020: Unlike the R package, the QIIME2 q2-phylogenize plugin now defaults to using the aligner vsearch (which is part of the QIIME2 distribution). The QIIME2 plugin will now also search your PATH for the aligner binary by default, instead of looking in `/usr/local/bin/`. You can still choose BURST or a binary in a specific directory by setting the options `--p-burst-bin` and `--p-burst-dir`, respectively. Finally, new options are exposed in the plugin including `--p-pctmin`, which may be useful to set differently for cases where fewer ASVs are detected.
 
# Installing

Run the following commands from within the QIIME 2 virtual environment:

```
python setup.py build
python setup.py install
```

If you are updating a previous version of the plugin, also call:

```
qiime dev refresh-cache
```

# Use

To see a full list of options, try:

```
qiime phylogenize run --help
```

The minimum options you must provide are:

 * **--i-table** (QIIME 2 artifact): A table of counts, relative abundances, or presence/absence values per amplicon sequence variant (ASV) across samples.
 * **--i-sequences** (QIIME 2 artifact): The actual denoised ASV sequences, from running DADA2 or Deblur.
 * **--m-metadata-file** (metadata table[s]): The metadata table, giving mappings of samples to environments and datasets.
 * **--p-which-envir** (text): A string giving the environment for which to calculate prevalence/specificity.
 * **--o-visualization** or **--output-dir** (directory): Where to output results.
 
You may also want to set:

 * **--p-burst-dir** (directory): This gives the location of where you've installed the [BURST](https://github.com/knights-lab/BURST) or vsearch binaries. Updated May 7, 2020: now defaults to searching your PATH for the vsearch/BURST binary instead of `/usr/local/bin`.
 * **--p-burst-bin** (directory): This gives the filename of the aligner you're using. Updated May 7, 2020: now defaults to `vsearch` since that should be installed for all QIIME2 users. You of course can still use BURST by providing `burst12` here.
 * **--p-env-column** (string): A string giving the name of the column in the metadata that maps samples to environments. Defaults to `env`.
 * **--p-pctmin** (float): A cutoff between 0 and 1 giving the minimum percentage of species that must be observed in a given phylum in order for that phylum to be analyzed. Defaults to 0.025 (2.5\%). This can be turned off entirely by setting it to 0, which may be useful for some cases where not a lot of ASVs are detected.

# Example

After following the instructions in the ["Moving Pictures"](https://docs.qiime2.org/2019.4/tutorials/moving-pictures/) tutorial and installing the q2-phylogenize plugin, try the following.

```
qiime phylogenize run --i-table table-dada2.qza --i-sequences rep-seqs-dada2.qza --m-metadata-file sample-metadata.tsv --p-which-envir gut --p-env-column BodySite --o-visualization phylogenize-test --verbose
```

(Note that you may have to let this run for a while. The "verbose" flag lets you better track how far along the analysis is. If you want, you can try increasing the number of cores with `--p-ncl` but be aware that this will also increase memory usage; for this test, we recommend around 2GB RAM/core.)

This calculates associations with prevalence in the gut within the samples provided in the tutorial. This should at least allow you to run *phylogenize* and visualize the output as follows (requires a web browser):

```
qiime tools view phylogenize-test.qzv
```
